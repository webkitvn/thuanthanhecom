module.exports = {
  content: ['**/**/*.{html,js,php}', './node_modules/tw-elements/dist/js/**/*.js'],
  plugins: [
    require('tw-elements/dist/plugin'),
    require('@tailwindcss/line-clamp'),
  ],
  theme: {
    extend: {
      colors: {
        'tt-green': '#8CC340',
        'tt-red': '#C31C28'
      },
    },
  },
}