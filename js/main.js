(function ($) {

  $(document).ready(function(){
    const headerHeight = $('#main-header').outerHeight();
    $('html').css('--header-height', headerHeight + 'px');
  })

  $(".menu-toggle-btn").on("click", function (e) {
    e.preventDefault();
    if (!$(this).hasClass("active")) {
      $(this).addClass("active");
      $("#mobile-menu").addClass("active");
      $("body").addClass("menu-actived");
    } else {
      $(this).removeClass("active");
      $("#mobile-menu").removeClass("active");
      $("body").removeClass("menu-actived");
    }
  });

  $('#mobile-menu .sub-menu').append('<button class="close-btn"><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1" stroke="currentColor" class="w-6 h-6"><path stroke-linecap="round" stroke-linejoin="round" d="M6.75 15.75L3 12m0 0l3.75-3.75M3 12h18" /></svg></button>');
  $('#menu-toogle').click(function(e) {
      e.preventDefault();
      $('#mobile-menu').toggleClass('active');
  });
  $('#mobile-menu .menu-item-has-children > a').on('click', function(e) {
      e.preventDefault();
      $(this).parent().find('> .sub-menu').addClass('active');
  });
  $('#mobile-menu .sub-menu').on('click', '.close-btn', function() {
      $(this).parent().removeClass('active');
  });

  $('.search-btn').on('click', function(e){
    e.preventDefault();
    $(this).toggleClass('active');
    $('.search-form').fadeToggle(300);
    $('.search-form input[type=search]').focus();
  });

  $(document).click(function (event) {
    if (!$(event.target).closest(".search-form, .search-btn").length) {
        $('.search-form').fadeOut();
        $('.search-btn').removeClass('active');
    }
  });

  $('body').on('click', '.buy_now_button', function(e){
    e.preventDefault();
    var thisParent = $(this).parents('form.cart');
    if($('.single_add_to_cart_button', thisParent).hasClass('disabled')) {
        $('.single_add_to_cart_button', thisParent).trigger('click');
        return false;
    }
    thisParent.addClass('devvn-quickbuy');
    $('.is_buy_now', thisParent).val('1');
    $('.single_add_to_cart_button', thisParent).trigger('click');
});

})(jQuery);