(function($) {
    $(".products, .product-summary").on('click', '.btnOrder', function() {
        var _this = $(this);
        if (!_this.hasClass("loading")) {
            _this.addClass("loading");
            var data = {
                action: "add_order_customer",
                dataID: _this.data("id"),
            };
            $.post(ajaxurl, data, function(html) {
                $(".icon_cart span").text(parseInt($(".icon_cart span").text()) + 1);
                if (_this.hasClass("orderNow")) {
                    window.location.href = CART_URL;
                } else {
                    _this.text("XEM GIỎ HÀNG").addClass("ok");
                }
            });
        } else {
            if (_this.hasClass("ok")) {
                window.location.href = CART_URL;
            }
        }
        return false;
    });
    $(".triaction").click(function() {
        var _this = $(this);
        $("#actionboom").val(_this.val());
    });
    $(".form_cart").on("submit", function() {
        var _this = $(this);
        if (!_this.hasClass("loading")) {
            _this.addClass("loading");
            var data = {
                action: "updatecart_customer",
                postdata: _this.serialize(),
            };
            $.post(ajaxurl, data, function(html) {
                window.location.href = CART_URL;
            });
        }
        return false;
    });

    $(".form_cart").on("change", "input.qty", function() {
        console.log("change");
        if (timeout !== undefined) {
            clearTimeout(timeout);
        }
        timeout = setTimeout(function() {
            $(".form_cart button[type=submit]").trigger("click");
            // trigger cart update
        }, 1000);
        // 1 second delay, half a second (500) seems comfortable too
    });

}
)(jQuery);

function down_quantity(elem) {
    var _this = jQuery(elem);
    var _n = parseInt(_this.parent(".quantity").find(".qty").val());
    if (_n > 1) {
        _n = _n - 1;
        _this.parent(".quantity").find(".qty").attr("value", _n);
    }
    //jQuery(".form_cart").submit();
    //fupdate_total_cart();
}
function up_quantity(elem) {
    var _this = jQuery(elem);
    var _n = parseInt(_this.parent(".quantity").find(".qty").val());
    _n = _n + 1;
    _this.parent(".quantity").find(".qty").attr("value", _n);
    //jQuery(".form_cart").submit();
    //fupdate_total_cart();
}
function delete_item_cart(_id) {
    var data = {
        action: "delete_order_customer",
        pid: _id,
    };
    jQuery.post(ajaxurl, data, function(html) {
        window.location.href = CART_URL;
    });
}


document.addEventListener( 'wpcf7mailsent', function( event ) {
    if ( '40' == event.detail.contactFormId ) {
        document.cookie = 'pcart=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;';
    }
}, false );

function eraseCookie(name) {   
    document.cookie = name+'=; Max-Age=0';  
}