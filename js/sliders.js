(function ($) {
	var homeBanners = new Swiper(".page-header-slider .swiper", {
		direction: "horizontal",
		loop: true,
		slidesPerView: 1,
		speed: 1000,
		autoplay: {
			delay: 8000,
		},
		// Navigation arrows
		navigation: {
			nextEl: ".page-header-slider .swiper-button-next",
			prevEl: ".page-header-slider .swiper-button-prev",
		},
	});

	var relatedPostsSlider = new Swiper(".related-posts .swiper-container", {
		direction: "horizontal",
		loop: false,
		slidesPerView: 2,
		spaceBetween: 15,
		speed: 300,
		autoplay: {
			delay: 5000,
		},
		// Navigation arrows
		navigation: {
			nextEl: ".swiper-button-next",
			prevEl: ".swiper-button-prev",
		},
		pagination: {
			el: ".swiper-pagination",
			clickable: true,
			renderBullet: function (index, className) {
				return '<span class="' + className + '">' + (index + 1) + "</span>";
			},
		},
		breakpoints: {
			640: {
				slidesPerView: 2,
				spaceBetween: 15,
			},
			768: {
				slidesPerView: 3,
				spaceBetween: 15,
			},
			1024: {
				slidesPerView: 3,
				spaceBetween: 30,
			},
		},
	});

	$(".products-slider .swiper").each(function (i, slider) {
		$pp_lg = $(this).data("pp-lg") ? $(this).data("pp-lg") : 4;
		$pp_md = $(this).data("pp-md") ? $(this).data("pp-md") : 3;
		$pp_sm = $(this).data("pp-sm") ? $(this).data("pp-sm") : 2;
		$count = i + 1;
		var imagesCarousel = new Swiper(slider, {
			direction: "horizontal",
			loop: false,
			loopAdditionalSlides: 1,
			slidesPerView: 2,
			slidesPerGroup: 2,
			spaceBetween: 20,
			speed: 1000,
			autoplay: {
				delay: 5000,
			},
			navigation: {
				nextEl: ".products-slider .swiper-button-next",
				prevEl: ".products-slider .swiper-button-prev",
			},
			pagination: {
				el: ".swiper-pagination",
			},
			breakpoints: {
				640: {
					slidesPerView: $pp_sm,
					slidesPerGroup: $pp_sm,
				},
				768: {
					slidesPerView: $pp_md,
					slidesPerGroup: $pp_md,
				},
				1024: {
					slidesPerView: $pp_lg,
				},
			},
		});
	});

	$(".image-slider .swiper").each(function (i, slider) {
		$pp_lg = $(this).data("pp-lg") ? $(this).data("pp-lg") : 1;
		$pp_md = $(this).data("pp-md") ? $(this).data("pp-md") : 1;
		$pp_sm = $(this).data("pp-sm") ? $(this).data("pp-sm") : 1;
		$count = i + 1;
		var imagesCarousel = new Swiper(slider, {
			direction: "horizontal",
			loop: true,
			loopAdditionalSlides: 1,
			slidesPerView: 1,
			spaceBetween: 10,
			speed: 1000,
			observeParents: true,
			autoplay: {
				delay: 5000,
			},
			navigation: {
				nextEl: ".image-slider .swiper .swiper-button-next",
				prevEl: ".image-slider .swiper .swiper-button-prev",
			},
			breakpoints: {
				640: {
					slidesPerView: $pp_sm,
					spaceBetween: 10,
				},
				768: {
					slidesPerView: $pp_md,
				},
				1024: {
					slidesPerView: $pp_lg,
					spaceBetween: 30,
				},
			},
		});
	});

	$(".posts-slider .swiper").each(function (i, slider) {
		$pp_lg = $(this).data("pp-lg") ? $(this).data("pp-lg") : 1;
		$pp_md = $(this).data("pp-md") ? $(this).data("pp-md") : 1;
		$pp_sm = $(this).data("pp-sm") ? $(this).data("pp-sm") : 1;
		$count = i + 1;
		var postsCarousel = new Swiper(slider, {
			direction: "horizontal",
			loop: true,
			loopAdditionalSlides: 1,
			slidesPerView: 1,
			spaceBetween: 30,
			speed: 300,
			navigation: {
				nextEl: ".posts-slider .swiper-button-next",
				prevEl: ".posts-slider .swiper-button-prev",
			},
			breakpoints: {
				640: {
					slidesPerView: $pp_lg,
				},
				768: {
					slidesPerView: $pp_md,
				},
				1024: {
					slidesPerView: $pp_sm,
				},
			},
		});
	});

	var productThumbs = new Swiper(".product-images .thumbnails .swiper", {
		loop: true,
		spaceBetween: 20,
		slidesPerView: 4,
		freeMode: true,
        watchSlidesProgress: true,
	});

	var productImages = new Swiper(".product-images .images .swiper", {
		loop: true,
		slidesPerView: 1,
		speed: 500,
		navigation: {
			nextEl: ".product-images .images .swiper-button-next",
			prevEl: ".product-images .images .swiper-button-prev",
		},
		thumbs: {
			swiper: productThumbs,
		},
	});

	


})(jQuery);
