<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     3.9.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $related_products ) : ?>

<section class="related products-slider lg:!mt-20">

    <?php
		$heading = apply_filters( 'woocommerce_product_related_products_heading', __( 'Related products', 'woocommerce' ) );

		if ( $heading ) :
			?>
    <h2 class="text-center text-tt-green lg:!text-6xl mb-4"><?php echo esc_html( $heading ); ?></h2>
    <?php endif; ?>
    <div class="products swiper">
        <div class="swiper-wrapper">
            <?php foreach ( $related_products as $related_product ) : ?>
            <div class="swiper-slide">
                <?php
					$post_object = get_post( $related_product->get_id() );

					setup_postdata( $GLOBALS['post'] =& $post_object ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited, Squiz.PHP.DisallowMultipleAssignments.Found

					wc_get_template_part( 'content', 'product' );
					?>
            </div>
            <?php endforeach; ?>
        </div>
        <div class="swiper-pagination static lg:hidden"></div>
    </div>
    <div
        class="swiper-button-prev hidden after:content-none w-[40px] h-[40px] rounded-full border border-current lg:flex justify-center items-center text-tt-green m-0 left-[-50px] translate-y-[-50%] hover:text-tt-green">
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor"
            class="w-6 h-6">
            <path stroke-linecap="round" stroke-linejoin="round" d="M10.5 19.5L3 12m0 0l7.5-7.5M3 12h18" />
        </svg>
    </div>
    <div
        class="swiper-button-next hidden after:content-none w-[40px] h-[40px] rounded-full border border-current lg:flex justify-center items-center text-tt-green m-0 right-[-50px] translate-y-[-50%] hover:text-tt-green">
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor"
            class="w-6 h-6">
            <path stroke-linecap="round" stroke-linejoin="round" d="M13.5 4.5L21 12m0 0l-7.5 7.5M21 12H3" />
        </svg>
    </div>
</section>
<?php
endif;

wp_reset_postdata();