<?php 
    global $product; 
    if(have_rows('product_faq', $product->get_id())) :
?>
<div id="accordion-faqs" class="faqs-wrapper">
    <?php $i=1; while(have_rows('product_faq', $product->get_id())) : the_row(); ?>
    <div class="rounded-none border border-t-0 border-l-0 border-r-0">
        <h2 class="mb-0 !block" id="flush-heading-<?php echo $i ?>">
            <button
                class="group relative flex w-full items-center rounded-none border-0 py-4 px-5 pl-0 text-left text-base text-tt-green font-semibold transition [overflow-anchor:none] hover:z-[2] focus:z-[3] focus:outline-none [&[data-te-collapse-collapsed]]:text-black text-xl"
                type="button" data-te-collapse-init <?php echo $i!==1 ? 'data-te-collapse-collapsed' : '' ?>
                data-te-target="#flush-collapse-<?php echo $i ?>" aria-expanded="false"
                aria-controls="flush-collapse-<?php echo $i ?>">
                <?php echo $i . '. ' . get_sub_field('question') ?>
                <span
                    class="ml-auto -mr-1 h-5 w-5 shrink-0 rotate-[-180deg] fill-[#336dec] transition-transform duration-200 ease-in-out group-[[data-te-collapse-collapsed]]:mr-0 group-[[data-te-collapse-collapsed]]:rotate-0 group-[[data-te-collapse-collapsed]]:fill-[#212529] motion-reduce:transition-none">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                        stroke="currentColor" class="h-6 w-6">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M19.5 8.25l-7.5 7.5-7.5-7.5" />
                    </svg>
                </span>
            </button>
        </h2>
        <div id="flush-collapse-<?php echo $i ?>" class="!visible <?php echo $i!=1 ? 'hidden' : '' ?> border-0"
            data-te-collapse-item data-te-collapse-<?php echo $i==1 ? 'show' : 'collapsed' ?>
            aria-labelledby="flush-heading-<?php echo $i ?>" data-te-parent="#accordion-faqs">
            <?php the_sub_field('answer') ?>
        </div>
    </div>
    <?php $i++; endwhile; ?>
</div>
<?php endif; ?>