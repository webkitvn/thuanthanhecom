<?php get_header() ?>
<section class="py-10">
    <div class="wrapper">
        <h1 class="entry-title text-tt-green text-3xl mb-6">
            <?php _e("Kết quả tìm kiếm", "mytheme") ?>: <span class="font-bold"><?php echo get_search_query() ?></span>
        </h1>
        <div class="results-content">
            <?php if(have_posts()) : ?>
            <?php while(have_posts()) : the_post();?>
                <div class="post max-w-4xl mb-6">
                    <h3 class="title flex flex-wrap gap-x-2">
                        <span class="block text-xs bg-gray-200 px-2 py-1 rounded w-fit whitespace-nowrap self-start">
                            <?php 
                                if($post->post_type == 'vigo_product'){
                                    echo __("Sản phẩm", "mytheme");
                                }
                                elseif($post->post_type == 'post'){
                                    echo __("Tin tức", "mytheme");
                                }
                            ?>
                        </span>
                        <a class="hover:text-tt-green font-bold" href="<?php the_permalink() ?>"><?php the_title() ?></a>
                    </h3>
                    <p class="text-sm mt-1"><?php echo get_the_excerpt() ?></p>
                </div>
            <?php endwhile; ?>
            <?php else : ?>
            <p class="text-xl"><?php _e("Không có kết quả phù hợp", "mytheme") ?></p>
            <?php endif; ?>
        </div>
        <?php wp_pagenavi() ?>
    </div>
</section>
<?php get_footer() ?>