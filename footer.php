    <footer id="footer" class="pt-16 pb-10 lg:pb-60 bg-left-bottom bg-cover relative">
        <div class="wrapper row lg:flex gap-20 justify-between relative z-10">
            <div class="left lg:min-w-[20%] lg:max-w-[30%] mb-8 lg:mb-0">
                <?php get_template_part('templates/footer/content', 'about') ?>
            </div>
            <div class="right">
                <div class="row lg:flex gap-20">
                    <?php get_template_part('templates/footer/content', 'menu') ?>
                    <?php get_template_part('templates/footer/content', 'contact') ?>
                </div>
                <div class="row lg:flex gap-20 mt-6">
                    <?php get_template_part('templates/footer/content', 'socials') ?>
                    <?php get_template_part('templates/footer/content', 'form') ?>
                </div>
            </div>
        </div>
        <?php get_template_part('templates/footer/content', 'totop') ?>
    </footer>

    <?php wp_footer() ?>
    <?php the_field('footer_scripts', 'option') ?>
    </body>

    </html>