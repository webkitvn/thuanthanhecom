<?php get_header() ?>
    <section class="py-10 border-t">
        <div class="wrapper">
            <?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>
        </div>
        <div class="wrapper !mt-10">
            <div class="product-detail grid grid-cols-1 lg:grid-cols-2 gap-6">
                <?php get_template_part('templates/product/single/single-images'); ?>
                <?php get_template_part('templates/product/single/single-summary'); ?>
            </div>
            <?php get_template_part('templates/product/single/tabs'); ?>
        </div>
    </section>
    <?php get_template_part('templates/product/single/related'); ?>
<?php get_footer() ?>