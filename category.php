<?php get_header() ?>
<?php get_template_part('templates/blocks/page-header') ?>
<section class="py-16">
    <div class="wrapper">
        <?php 
            $args = array(
                'text' => "Tin tức"
            );
            get_template_part('templates/content', 'section-title', $args);
        ?>
        <?php 
            $cats = get_terms(array(
                'taxonomy' => 'category',
                'parent' => 1,
                'hide_empty' => true
            ));
            if($cats) :
        ?>
        <ul class="nav nav-tabs custom-nav-tabs flex lg:justify-center list-none border-b pl-0 mt-4 mb-4 w-full lg:w-fit mx-auto snap-x snap-mandatory overflow-x-auto" id="tabs-tab" role="tablist" data-te-nav-ref>
            <?php $i = 1; foreach($cats as $cat) : ?>
            <li class="nav-item snap-start" role="presentation">
            <a href="#tabs-<?php echo $cat->term_id ?>"
                    class="nav-link text-center text-sm lg:text-lg block w-fit whitespace-nowrap font-bold border-x-0 border-t-0 border-b-2 border-transparent px-6 py-3 hover:border-transparent hover:bg-gray-100 focus:border-transparent"
                    data-te-toggle="pill"
                    data-te-target="#tabs-<?php echo $cat->term_id ?>" 
                    role="tab" 
                    <?php echo $i==1 ? 'data-te-nav-active' : '' ?>
                    aria-controls="tabs-<?php echo $cat->term_id ?>"
                    aria-selected="<?php echo $i == 1 ? 'true' : 'false' ?>"><?php echo $cat->name ?></a>
            </li>
            <?php $i++; endforeach; ?>
        </ul>
        <div class="tab-content tab-posts mt-10" id="tabs-tabContent">
            <?php $i = 1; foreach($cats as $cat) : ?>
                <div class="hidden <?php echo $i == 1 ? 'opacity-100' : 'opacity-0' ?> transition-opacity duration-150 ease-linear data-[te-tab-active]:block"
                id="tabs-<?php echo $cat->term_id ?>" 
                role="tabpanel"
                aria-labelledby="tabs-<?php echo $cat->term_id ?>-tab"
                <?php echo $i==1 ? 'data-te-tab-active' : '' ?>>
                <?php echo do_shortcode('[ajax_load_more id="'.$cat->term_id.'" container_type="div" css_classes="posts grid grid-cols-2 lg:grid-cols-4 gap-4 lg:gap-8 lg:gap-y-10" post_type="post" posts_per_page="12" category="'.$cat->slug.'" transition_container="false"]') ?>
            </div>
            <?php $i++; endforeach; ?>
        </div>
        <?php endif; ?>
    </div>
</section>
<?php get_footer() ?>