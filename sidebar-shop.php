<sidebar class="sidebar product-sidebar bg-gray-50 p-6 rounded-md">
    <?php
        if(is_active_sidebar( 'shop' )){
            dynamic_sidebar('shop');
        }
    ?>
</sidebar>