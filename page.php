<?php get_header() ?>
<?php while(have_posts()) : the_post(); ?>
<div class="page-header-slider py-8 bg-gray-50">
    <div class="wrapper">
        <?php if (function_exists('rank_math_the_breadcrumbs')) : ?>
        <div id="breadcrumbs"><?php rank_math_the_breadcrumbs(); ?></div>
        <?php endif; ?>
        <h1 class="entry-title mt-2 font-bold text-2xl"><?php the_title() ?></h1>
    </div>
</div>
<div id="main-content" class="py-6">
    <div class="wrapper">
        <?php the_content() ?>
    </div>
</div>
<?php endwhile; ?>
<?php get_footer() ?>