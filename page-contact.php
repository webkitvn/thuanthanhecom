<?php /* Template name: Liên hệ */ ?>
<?php get_header() ?>
<?php get_template_part('templates/blocks/page-header') ?>
<section class="py-10">
    <div class="wrapper">
        <?php 
            $args = array(
                'text' => get_the_title()
            );
            get_template_part('templates/content', 'section-title', $args);
        ?>
        <div class="grid lg:grid-cols-2 gap-8">
            <div class="contact-form mt-10">
                <h3 class="font-bold text-tt-green text-2xl mb-4"><?php _e("Để lại thông tin cho chúng tôi", "mytheme") ?></h3>
                <div class="form-wrapper">
                    <?php echo do_shortcode('[contact-form-7 id="24" title="Liên hệ"]') ?>
                </div>
            </div>
            <div class="contact-info mt-10">
                <h3 class="font-bold text-tt-green text-2xl mb-4"><?php _e("Liên hệ trực tiếp với chúng tôi", "mytheme") ?></h3>
                <?php the_content() ?>
            </div>
        </div>
    </div>
</section>
<?php if(get_field('google_map', 'option')) : ?>
<div id="map">
    <?php the_field('google_map', 'option') ?>
</div>
<?php endif; ?>
<?php get_footer() ?>