<?php /* Template name: Giới thiệu */ ?>
<?php get_header() ?>
<?php //get_template_part('templates/blocks/page-header-slider') ?>
<section class="section py-10">
    <div class="wrapper">
        <div class="wrapper text-center">
            <?php 
                $args = array(
                    'text' => get_the_title()
                );
                get_template_part('templates/content', 'section-title', $args);
            ?>
        </div>
        <ul class="nav nav-tabs custom-nav-tabs flex justify-center list-none border-b pl-0 w-full lg:w-fit mx-auto mt-6"
            id="tabs-tab" role="tablist" data-te-nav-ref>
            <li class="nav-item" role="presentation">
                <a href="#tabs-1"
                    class="nav-link text-base lg:text-lg block font-bold text-center border-x-0 border-t-0 border-b-2 border-transparent px-6 py-3 hover:border-transparent hover:bg-gray-100 focus:border-transparent active"
                    data-te-toggle="pill" 
                    data-te-target="#tabs-1" 
                    role="tab" 
                    aria-controls="tabs-1"
                    aria-selected="true" 
                    data-te-nav-active><?php _e("Giới thiệu công ty", "mytheme") ?></a>
            </li>
            <li class="nav-item" role="presentation">
                <a href="#tabs-2"
                    class="nav-link text-base lg:text-lg block font-bold text-center border-x-0 border-t-0 border-b-2 border-transparent px-6 py-3 hover:border-transparent hover:bg-gray-100 focus:border-transparent"
                    data-te-toggle="pill" 
                    data-te-target="#tabs-2" 
                    role="tab" 
                    aria-controls="tabs-2"
                    aria-selected="false"><?php _e("Câu chuyện thương hiệu", "mytheme") ?></a>
            </li>
        </ul>
    </div>
    <div class="tab-content tab-posts" id="tabs-tabContent">
        <div class="hidden opacity-100 transition-opacity duration-150 ease-linear data-[te-tab-active]:block" 
        id="tabs-1" 
        role="tabpanel" 
        aria-labelledby="tabs-1-tab"
        data-te-tab-active>
            <?php 
                $args = array(
                    'title' => get_field('about_title'),
                    'content' => get_field('about_content'),
                    'custom_content' => get_field('about_custom_content')
                );
                get_template_part('templates/blocks/image-text','', $args);
                if(get_field('about_image_slider')){
                    $args = array('images'=>get_field('about_image_slider'));
                    get_template_part('templates/blocks/image-slider','', $args); 
                }
            ?>
        </div>
        <div class="hidden opacity-0 transition-opacity duration-150 ease-linear data-[te-tab-active]:block" 
        id="tabs-2"
        role="tabpanel" 
        aria-labelledby="tabs-2-tab">
            <?php 
                $args = array(
                    'title' => get_field('story_title'),
                    'content' => get_field('story_content'),
                    'custom_content' => get_field('story_custom_content')
                );
                get_template_part('templates/blocks/image-text','', $args);
                if(get_field('story_image_slider')){
                    $args = array('images'=>get_field('story_image_slider'));
                    get_template_part('templates/blocks/image-slider','',$args); 
                }
            ?>
        </div>
    </div>
</section>
<?php get_template_part('templates/blocks/about-features') ?>
<?php get_template_part('templates/home/content-products') ?>
<?php get_footer() ?>