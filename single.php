<?php get_header() ?>
<section class="border-t py-10">
    <div class="wrapper">
        <?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>
    </div>
    <div class="wrapper grid grid-cols-1 lg:grid-cols-3 gap-y-6 lg:gap-x-6 !mt-8">
        <div class="main-content col-span-2">
            <h1 class="entry-title text-2xl lg:text-4xl font-bold">
                <?php the_title() ?>
            </h1>
            <div class="meta text-gray-400 text-sm font-bold mt-4">
                <div class="date"><?php the_time('d/m/Y') ?></div>
            </div>
            <div class="entry-content mt-6">
                <?php the_content() ?>
            </div>
        </div>
        <?php get_template_part('templates/post/sidebar') ?>
    </div>
</section>
<?php get_footer() ?>