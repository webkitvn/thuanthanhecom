<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Be+Vietnam+Pro:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">
    <?php wp_head(); ?>
    <?php the_field('header_scripts', 'option') ?>
</head>

<body <?php body_class(); ?>>
    <?php wp_body_open() ?>
    <header id="main-header" class="bg-white" data-headroom>
        <div class="wrapper flex flex-wrap lg:flex-nowrap items-center !pb-4 lg:!pb-0">
            <a href="<?php bloginfo('home') ?>"
                class="logo flex items-end w-[120px] lg:w-[150px] flex-none order-2 lg:-order-none">
                <?php 
                    $custom_logo_id = get_theme_mod( 'custom_logo' );
                    if ( has_custom_logo() ) {
                        echo wp_get_attachment_image($custom_logo_id, 'medium', false, array('alt'=>get_bloginfo('name'), 'loading'=>'eager'));
                    } else {
                        echo '<h1>' . get_bloginfo('name') . '</h1>';
                    }
                ?>
            </a>
            <nav class="nav flex flex-wrap lg:justify-end flex-auto lg:ml-4 gap-y-4 w-full lg:w-auto">
                <?php get_template_part('templates/header/top-nav') ?>
                <?php get_template_part('templates/header/bottom-nav') ?>
            </nav>
            <button class="menu-toggle-btn self-center ml-auto" aria-label="MENU">
                <span></span><span></span><span></span>
            </button>
        </div>
    </header>
    <?php get_template_part('templates/header/mobile-menu') ?>