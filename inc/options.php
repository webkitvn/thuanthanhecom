<?php 
    acf_add_options_page(array(
        'page_title' 	=> 'Cài đặt chung',
        'menu_title'	=> 'Cài đặt chung',
        'menu_slug' 	=> 'theme-general-settings',
        'capability'	=> 'edit_posts',
        'redirect'		=> false,
        'position'      => '4'
    ));

    acf_add_options_sub_page(array(
        'page_title' 	=> 'Scripts',
        'menu_title'	=> 'Scripts',
        'parent_slug'	=> 'theme-general-settings',
    ));
?>