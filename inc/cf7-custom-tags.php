<?php

    function wpshore_wpcf7_before_send_mail($array) {
        global $wpdb;
        if(wpautop($array['body']) == $array['body']) // The email is of HTML type
            $lineBreak = "</p><br/>";
        else
            $lineBreak = "</p>\n";
        $html = '';
        if(isset($_COOKIE['pcart']) && $_COOKIE['pcart']!=""){
            $check_car_json = json_decode(stripslashes($_COOKIE['pcart']),true);
            if(count($check_car_json) > 0){
                $html .= '<table style="border-collapse:collapse;border-spacing:0;width:100%;" class="tg"><thead><tr><th style="border-color:inherit;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;font-weight:bold;overflow:hidden;padding:10px 5px;text-align:left;vertical-align:top;word-break:normal">Sản phẩm</th><th style="border-color:inherit;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;font-weight:bold;overflow:hidden;padding:10px 5px;text-align:left;vertical-align:top;word-break:normal">MSP</th><th style="border-color:inherit;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;font-weight:bold;overflow:hidden;padding:10px 5px;text-align:left;vertical-align:top;word-break:normal">Số lượng</th><th style="border-color:inherit;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;font-weight:bold;overflow:hidden;padding:10px 5px;text-align:left;vertical-align:top;word-break:normal">Đơn giá</th><th style="border-color:inherit;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;font-weight:bold;overflow:hidden;padding:10px 5px;text-align:left;vertical-align:top;word-break:normal">Thành tiền</th></tr></thead>';
            }
            $pcarts = $check_car_json;
            $total = 0;
            foreach ($pcarts as $key => $item){
                $regular_price = floatval(get_field('product_regular_price', $item['id']));
                $sale_price = floatval(get_field('product_sale_price', $item['id']));
                if($sale_price && $sale_price < $regular_price){
                    $total += $sale_price * $item['count'];
                }
                else{
                    $total += $regular_price * $item['count'];
                }
                $html .= '<tr>';

                $html .= '<td style="border-color:inherit;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;overflow:hidden;padding:10px 5px;text-align:left;vertical-align:top;word-break:normal"><h4>' . get_the_title($item['id']) . '</h4></td>';

                $html .= '<td style="border-color:inherit;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;overflow:hidden;padding:10px 5px;text-align:left;vertical-align:top;word-break:normal">' . get_field('product_sku', $item['id']) . '</td>';

                $html .= '<td style="border-color:inherit;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;overflow:hidden;padding:10px 5px;text-align:left;vertical-align:top;word-break:normal">' . $item['count'] . '</td>';

                $html .= '<td style="border-color:inherit;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;overflow:hidden;padding:10px 5px;text-align:left;vertical-align:top;word-break:normal"><span class="price">';

                if($sale_price && $sale_price < $regular_price){
                    $html .= '<span class="amount">' .number_format($sale_price, 0, ',', '.') . '</span><span class="currency">đ</span>';
                }
                else{
                    $html .= '<span class="amount">' . number_format($regular_price, 0, ',', '.') . '</span><span class="currency">đ</span>';
                }

                $html .= '</span></td>';

                $html .= '<td style="border-color:inherit;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;overflow:hidden;padding:10px 5px;text-align:left;vertical-align:top;word-break:normal"><span class="price">';

                if($sale_price && $sale_price < $regular_price){
                    $html .= '<span class="amount">' . number_format($sale_price*$item['count'], 0, ',', '.') . '</span><span class="currency">đ</span>';
                }
                else{
                    $html .= '<span class="amount">' . number_format($regular_price*$item['count'], 0, ',', '.') . '</span><span class="currency">đ</span>';
                }

                $html .= '</span></td>';
                
                $html .= '</tr>';
            }

            $html .= '<tr><td style="border-color:inherit;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;overflow:hidden;padding:10px 5px;text-align:left;vertical-align:top;word-break:normal">Tổng</td><td style="border-color:inherit;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:20px;overflow:hidden;padding:10px 5px;text-align:left;font-weight:bold;vertical-align:top;word-break:normal" colspan="4"><span class="price font-bold text-2xl"><span class="amount">' . number_format($total, 0, ',', '.') . '</span><span class="currency">đ</span></span></td></tr>';
            $html .= '</table>';
        }
        $array['body'] = str_replace('[site_cart]', $html, $array['body']);
        return $array;
    }
    add_filter('wpcf7_mail_components', 'wpshore_wpcf7_before_send_mail');