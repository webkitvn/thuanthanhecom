<?php
add_filter('wp_nav_menu_objects', 'my_wp_nav_menu_objects', 10, 2);

function my_wp_nav_menu_objects( $items, $args ) {
    
    // loop
    foreach( $items as &$item ) {
        
        // vars
        $image = get_field('menu_image', $item);
        
        
        // append icon
        if( $image ) {
            
            $item->title = wp_get_attachment_image($image['id'], 'medium') . $item->title;
            
        }
        
    }
    
    
    // return
    return $items;
    
}