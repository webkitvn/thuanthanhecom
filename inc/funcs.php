<?php
    function image_with_link($link, $imgID, $imgSize, $class = ''){
        if($link && $link['url']){
            echo sprintf("<a href='%s' target='%s' aria-label='%s' class='%s'>", $link['url'], $link['target'], $link['title'], $class);
        }
            echo wp_get_attachment_image($imgID, $imgSize);
        if($link && $link['url']){
            echo '</a>';
        }
    }