<?php
// Show lowest price of variable products in shop page or product categories
add_filter( 'woocommerce_variable_sale_price_html', 'custom_variation_price_format', 10, 2 );
add_filter( 'woocommerce_variable_price_html', 'custom_variation_price_format', 10, 2 );

function custom_variation_price_format( $price, $product ) {
    // Check if not a single product page
    if ( ! is_singular( 'product' ) ) {
        // Main Price
        $prices = array( $product->get_variation_price( 'min', true ), $product->get_variation_price( 'max', true ) );
        $price = $prices[0] !== $prices[1] ? wc_price( $prices[0] ) : wc_price( $prices[0] );

        // Sale Price
        $prices = array( $product->get_variation_regular_price( 'min', true ), $product->get_variation_regular_price( 'max', true ) );
        sort( $prices );
        $saleprice = $prices[0] !== $prices[1] ? wc_price($prices[0]) : wc_price( $prices[0] );

        if ( $price !== $saleprice ) {
            $price = '<del>' . $saleprice . $product->get_price_suffix() . '</del> <ins>' . $price . $product->get_price_suffix() . '</ins>';
        }
    }
    return $price;
}
