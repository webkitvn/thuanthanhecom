<?php
    add_filter( 'woocommerce_product_tabs', 'woo_new_product_tab' );
    function woo_new_product_tab( $tabs ) {
        $tabs['qcdg_tab'] = array(
            'title' 	=> __( 'Quy cách đóng gói', 'mytheme' ),
            'priority' 	=> 50,
            'callback' 	=> 'woo_qcdg_tab_content' );
        $tabs['hsd_tab'] = array(
            'title' 	=> __( 'Hạn sử dụng', 'mytheme' ),
            'priority' 	=> 50,
            'callback' 	=> 'woo_hsd_tab_content' );
        $tabs['faq_tab'] = array(
            'title' 	=> __( 'FAQs', 'mytheme' ),
            'priority' 	=> 50,
            'callback' 	=> 'woo_faq_tab_content' );
        return $tabs; 
    }

function woo_qcdg_tab_content() {
    global $product;
    if(get_field("product_packing", $product->get_id())){
        echo get_field("product_packing", $product->get_id());
    }
}
function woo_hsd_tab_content() {
    global $product;
    if(get_field("product_expiry", $product->get_id())){
        echo "<div class='entry-content'>" . get_field("product_expiry", $product->get_id()) . "</div>";
    }
}
function woo_faq_tab_content() {
    wc_get_template_part('single-product/tabs/faqs');
}