<?php
    function wc_track_product_view_always() {
        if ( ! is_singular( 'product' ) /* xnagyg: remove this condition to run: || ! is_active_widget( false, false, 'woocommerce_recently_viewed_products', true )*/ ) {
            return;
        }
    
        global $post;
    
        if ( empty( $_COOKIE['woocommerce_recently_viewed'] ) ) { // @codingStandardsIgnoreLine.
            $viewed_products = array();
        } else {
            $viewed_products = wp_parse_id_list( (array) explode( '|', wp_unslash( $_COOKIE['woocommerce_recently_viewed'] ) ) ); // @codingStandardsIgnoreLine.
        }
    
        // Unset if already in viewed products list.
        $keys = array_flip( $viewed_products );
    
        if ( isset( $keys[ $post->ID ] ) ) {
            unset( $viewed_products[ $keys[ $post->ID ] ] );
        }
    
        $viewed_products[] = $post->ID;
    
        if ( count( $viewed_products ) > 15 ) {
            array_shift( $viewed_products );
        }
    
        // Store for session only.
        wc_setcookie( 'woocommerce_recently_viewed', implode( '|', $viewed_products ) );
    }
    
    remove_action('template_redirect', 'wc_track_product_view', 20);
    add_action( 'template_redirect', 'wc_track_product_view_always', 20 );

    function recent_viewed_products(){
        wc_get_template_part('single-product/content', 'recent-viewed');
    }