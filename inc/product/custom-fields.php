<?php
    // Display Fields
add_action('woocommerce_product_options_inventory_product_data', 'woocommerce_product_custom_fields');

// Save Fields
add_action('woocommerce_process_product_meta', 'woocommerce_product_custom_fields_save');


function woocommerce_product_custom_fields()
{
    global $woocommerce, $post;
    echo '<div class="product_custom_field">';
    // Custom Product Text Field
    woocommerce_wp_text_input(
        array(
            'id' => '_custom_product_text',
            //'placeholder' => 'Custom Product Text',
            'label' => __('Custom Product Text', 'woocommerce'),
            'desc_tip' => 'true'
        )
    );
    echo '</div>';

}

function woocommerce_product_custom_fields_save($post_id)
{
    $woocommerce_custom_product_text = $_POST['_custom_product_text'];
    update_post_meta($post_id, '_custom_product_text', esc_attr($woocommerce_custom_product_text));
}