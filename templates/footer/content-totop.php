<a href="#" id="totop" class="inline-block absolute top-[-20px] right-[15px] lg:right-[100px] text-center text-tt-green text-sm">
    <span class="icon rounded-full bg-tt-green text-white w-[40px] h-[40px] flex justify-center items-center mb-1">
        <svg class="h-[2em]" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
            <path stroke-linecap="round" stroke-linejoin="round" d="M4.5 15.75l7.5-7.5 7.5 7.5" />
        </svg>
    </span>
    <span>UP</span>
</a>