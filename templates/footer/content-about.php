<?php 
    if(get_field('footer_logo', 'option')) :
        $logo = get_field('footer_logo', 'option');
?>
<a href="<?php bloginfo('url') ?>" class="logo block mb-2 lg:mb-0">
    <?php echo wp_get_attachment_image($logo['id'], 'medium', false, array('class'=>'w-[269px] h-auto')) ?>
</a>
<?php endif; ?>
<?php if(get_field('footer_about', 'option')) : ?>
<div class="entry-content mt-4 font-[500]">
    <p><?php the_field('footer_about', 'option') ?></p>
</div>
<?php endif; ?>