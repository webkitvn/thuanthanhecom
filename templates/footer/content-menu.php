<div class="col min-w-[30%] mb-8 lg:mb-0">
    <?php if(has_nav_menu('footer_menu')) : ?>
    <h3 class="footer-title font-bold border-b border-white pb-2">Thuận Thành</h3>
    <?php wp_nav_menu(array(
        'theme_location' => 'footer_menu',
        'menu_class' => 'menu mt-4 font-[500]',
        'container' => ''
    )) ?>
    <?php endif; ?>
</div>