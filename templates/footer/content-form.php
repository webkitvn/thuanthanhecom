<div class="col min-w-[30%]">
    <h3 class="footer-title font-bold border-b border-white pb-2"><?php _e("Đăng ký nhận tin", "mytheme") ?></h3>
    <div class="form-wrapper mt-4">
        <?php echo do_shortcode('[contact-form-7 id="25" title="Đăng ký nhận tin"]') ?>
    </div>
    <p class="copyright text-xs text-gray-500 mt-6">
        &copy2022 - Bản quyền thuộc về Thuận Thành | Design by Amwind
    </p>
</div>