<div class="col min-w-[30%] mb-8 lg:mb-0">
    <h3 class="footer-title font-bold border-b border-white pb-2"><?php _e("Liên hệ", "mytheme") ?></h3>
    <ul class="icon-list mt-4 font-[500]">
        <?php if(get_field('address', 'option')) : ?>
        <li class="flex gap-x-2">
            <img class="h-4" src="<?php echo get_stylesheet_directory_uri() ?>/img/address.svg" alt="address" width="14" height="14" loading="lazy">
            <span><?php the_field('address', 'option') ?></span>
        </li>
        <?php endif; ?>
        <?php if(get_field('email', 'option')) : ?>
        <li class="flex gap-x-2">
            <img class="h-4" src="<?php echo get_stylesheet_directory_uri() ?>/img/email.svg" alt="email" width="18" height="14" loading="lazy">
            <span><a href="mailto:<?php the_field('email', 'option') ?>"><?php the_field('email', 'option') ?></a></span>
        </li>
        <?php endif; ?>
        <?php if(get_field('phone_number', 'option')) : $phone = get_field('phone_number', 'option'); ?>
        <li class="flex gap-x-2">
            <img class="h-4" src="<?php echo get_stylesheet_directory_uri() ?>/img/phone.svg" alt="phone" width="14" height="14" loading="lazy">
            <span><a class="text-black" href="tel:<?php echo $phone['phone'] ?>"><?php echo $phone['text'] ?></a></span>
        </li>
        <?php endif; ?>
        <?php if(get_field('website', 'option')) : $website = get_field('website', 'option') ?>
        <li class="flex gap-x-2">
            <img class="h-4" src="<?php echo get_stylesheet_directory_uri() ?>/img/website.svg" alt="website" width="16" height="14" loading="lazy">
            <span><a class="text-black" href="<?php echo $website['link'] ?>"><?php echo $website['text'] ?></a></span>
        </li>
        <?php endif; ?>
    </ul>
</div>