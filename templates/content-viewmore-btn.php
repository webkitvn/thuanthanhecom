<?php 
    $text = $args['text'];
    $color = $args['color'];
    $link = $args['link'];
?>
<a href="<?php echo isset($link['url']) ? $link['url'] : '#' ?>" class="viewmore-btn mt-6 inline-flex items-center gap-x-4 w-fit text-<?php echo $color ?>">
    <span class="font-bold block w-fit"><?php echo $text ?></span>
    <img class="style-svg w-[50px]"
        src="<?php echo get_stylesheet_directory_uri() ?>/img/more-arrow.svg" alt="more">
</a>