<?php 
    $text = $args['text'];
?>
<div class="section-title">
    <h2 class="entry-title text-center font-bold text-3xl lg:text-5xl text-tt-green mb-2"><?php echo $text ?></h2>
</div>  