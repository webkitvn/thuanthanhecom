<section class="py-8">
    <div class="wrapper">
        <?php if($args['title']) : ?>
        <h2 class="font-bold text-tt-green text-2xl text-center">
            <?php echo $args['title'] ?>
        </h2>
        <?php endif; ?>
        <?php if($args['content']) : ?>
        <div class="entry-content text-center max-w-2xl mx-auto mt-4">
            <?php echo $args['content'] ?>
        </div>
        <?php endif; ?>
        <?php if($args['custom_content']) : ?>
        <div class="image-text">
            <?php 
                foreach($args['custom_content'] as $item) : 
                    $img = $item['image'];
                    $content = $item['content'];
            ?>
            <div class="item mt-6 lg:flex flex-wrap justify-between items-center lg:even:flex-row-reverse">
                <?php if($img) : ?>
                <div class="image lg:w-[48%] relative rounded-lg overflow-hidden">
                    <?php echo wp_get_attachment_image($img['id'], 'medium_large', false, array('class'=>'w-full rounded-lg')) ?>
                </div>
                <?php endif; ?>
                <div class="text-box mt-4 lg:mt-0 lg:w-[48%] flex items-center">
                    <div class="entry-content">
                        <?php echo $content ?>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
        <?php endif; ?>
    </div>
</section>