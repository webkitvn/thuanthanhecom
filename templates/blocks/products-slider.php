<section class="products-slider py-10 bg-gray-50">
    <div class="wrapper text-center relative">
        <?php 
            $args = array(
                'text' => "Sản phẩm"
            );
            get_template_part('templates/content', 'section-title', $args);
        ?>
        <div class="entry-content max-w-2xl mx-auto mt-4">
            <p>Chúng tôi phấn đấu với mục tiêu trở thành một trong những công ty chuyên cung cấp các sản phẩm về hạt
                điều hàng đầu trong cả nước, trong suốt quá trình hoạt động. Sản phẩm của Vigo- nuts luôn đảm bảo chất
                lượng và mang đến sự an tâm cho khách hàng.</p>
        </div>
    </div>
    <?php 
        $query = new WP_Query(array(
            'post_type' => 'vigo_product',
            'posts_per_page' => 10
        ));
    ?>
    <div class="wrapper">
        <div class="swiper products mt-10">
            <div class="swiper-wrapper">
                <?php 
                    while($query->have_posts()) : $query->the_post();
                ?>
                <div class="swiper-slide">
                    <?php get_template_part('templates/product/content', 'product') ?>
                </div>
                <?php endwhile; wp_reset_query(); ?>
            </div>
            <div class="swiper-pagination static lg:hidden"></div>
        </div>
        <div
            class="swiper-button-prev hidden after:content-none w-[40px] h-[40px] rounded-full border border-current lg:flex justify-center items-center text-tt-green m-0 left-[-50px] translate-y-[-50%] hover:text-tt-green">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                stroke="currentColor" class="w-6 h-6">
                <path stroke-linecap="round" stroke-linejoin="round" d="M10.5 19.5L3 12m0 0l7.5-7.5M3 12h18" />
            </svg>
        </div>
        <div
            class="swiper-button-next hidden after:content-none w-[40px] h-[40px] rounded-full border border-current lg:flex justify-center items-center text-tt-green m-0 right-[-50px] translate-y-[-50%] hover:text-tt-green">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                stroke="currentColor" class="w-6 h-6">
                <path stroke-linecap="round" stroke-linejoin="round" d="M13.5 4.5L21 12m0 0l-7.5 7.5M21 12H3" />
            </svg>
        </div>
    </div>
    <div class="text-center">
        <?php 
            $args = array(
                'text' => __("Xem tất cả", "mytheme"),
                'color' => 'tt-green',
                'link' => array()
            );
        get_template_part('templates/content', 'viewmore-btn', $args) ?>
    </div>
</section>