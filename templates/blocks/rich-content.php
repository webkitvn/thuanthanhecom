<section class="rich-content py-10">
    <div class="wrapper">
        <div class="entry-content max-w-2xl mx-auto">
            <p class="mb-4">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Reiciendis quos eligendi delectus perspiciatis dolorum ex modi praesentium eum. Nihil corporis atque illo quibusdam! Fuga, minus. Temporibus, aut. Perferendis, earum odit laboriosam nemo, quia inventore quod quas quisquam itaque voluptatibus quo ea? Architecto, harum.</p>
            <p class="mb-4">Vel aperiam id maxime dolores amet quae explicabo voluptatum eligendi placeat eaque, nisi ea officiis quod sit praesentium! Nulla cumque qui libero, sapiente suscipit iure voluptate at velit nemo minus, ipsum, aliquam distinctio culpa dolores! Voluptates nisi ratione ipsam fugiat architecto veniam impedit corrupti, quod quis, possimus accusamus.</p>
            <p class="mb-4">Iusto, praesentium unde! Consequuntur dolore, fugit amet laboriosam, odit minus nemo dignissimos repudiandae temporibus tempore excepturi voluptatum voluptatibus molestiae voluptatem! Harum repellendus velit a sunt eius animi odit ipsam nemo, tempora doloribus, saepe voluptate, aperiam voluptatum sit molestiae error eligendi fugit exercitationem quibusdam? Cupiditate, vel corrupti? Doloribus, magni saepe excepturi maiores voluptate dolore impedit.</p>
        </div>
    </div>
</section>