<section class="page-header relative">
    <div class="lg:grid lg:grid-cols-3">
        <div class="text-box py-8 px-[15px] lg:col-span-1 lg:p-6 lg:py-36 lg:pl-[50px] bg-tt-green text-white"
            data-mh="page-header-slider-text-box">
            <h1 class="entry-title font-bold text-3xl lg:text-6xl mb-4">
                <?php 
                    if(is_post_type_archive( 'vigo_product' )){
                        _e("Sản phẩm", "mytheme");
                    }
                    elseif(is_tax() || is_category()){
                        single_cat_title();
                    }
                    elseif(is_singular()){
                        the_title();
                    }
                ?>
            </h1>
            <?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>
        </div>
        <div class="img-holder relative overflow-hidden hidden lg:block lg:col-span-2">
            <?php 
                $img = get_field('header_cover_image', 'option');
                if($img) :
            ?>
            <?php
                    echo wp_get_attachment_image( $img['id'], 'full', false, array('class'=>'absolute w-full h-full top-0 left-0 object-cover') ); 
            ?>
            <?php else : ?>
                <img src="https://source.unsplash.com/1600x600?background" alt="cover image" loading="lazy" width="1600" height="600">
            <?php endif; ?>
        </div>
    </div>
</section>