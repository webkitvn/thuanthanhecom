<?php $i = 1; if(have_rows('vigo_banners')) : ?>
<section class="grid grid-cols-1 lg:grid-cols-2 gap-4 py-6 px-[15px] lg:px-0">
    <?php 
        while(have_rows('vigo_banners')) : the_row(); 
        $img = get_sub_field('image');
        $text_1 = get_sub_field('text_1');
        $text_2 = get_sub_field('text_2');
        $text_3 = get_sub_field('text_3');
        $link = get_sub_field('link');
    ?>
    <?php if($i % 2 != 0) : ?>
    <div class="item relative pt-20 pb-8 px-4 lg:pl-20 lg:pr-8 rounded-md lg:rounded-none odd:rounded-r-md even:rounded-l-md overflow-hidden" data-mh="banner-item">
        <?php if($img){
            echo wp_get_attachment_image($img['id'], 'medium_large', false, array('class'=>'absolute top-0 left-0 w-full h-full object-cover'));
        } ?>
        <div class="text-box relative z-20 text-white">
            <?php if($text_1) : ?>
            <p><?php echo $text_1; ?></p>
            <?php endif; ?>
            <?php if($text_2) : ?>
            <h3 class="font-bold text-3xl"><?php echo $text_2; ?></h3>
            <?php endif; ?>
            <?php if($text_3) : ?>
            <h2 class="font-bold text-4xl mt-4">Giảm giá 10%</h2>
            <?php endif; ?>
            <?php 
                $args = array(
                    'color' => 'white',
                    'text' => __("Xem thêm", "mytheme"),
                    'link' => $link
                );
                get_template_part('templates/content', 'viewmore-btn', $args);
            ?>
        </div>
    </div>
    <?php else : ?>
    <div class="item relative pt-20 pb-8 px-4 lg:pl-20 lg:pr-8 rounded-md lg:rounded-none odd:rounded-r-md even:rounded-l-md overflow-hidden" data-mh="banner-item">
        <?php if($img){
            echo wp_get_attachment_image($img['id'], 'medium_large', false, array('class'=>'absolute top-0 left-0 w-full h-full object-cover'));
        } ?>
        <div class="text-box relative z-20 text-white">
            <?php if($text_1) : ?>
            <p><?php echo $text_1; ?></p>
            <?php endif; ?>
            <?php if($text_2) : ?>
            <h3 class="font-bold text-3xl"><?php echo $text_2; ?></h3>
            <?php endif; ?>
            <?php if($text_3) : ?>
            <h2 class="font-bold text-4xl mt-4">Giảm giá 10%</h2>
            <?php endif; ?>
            <?php 
                $args = array(
                    'color' => 'white',
                    'text' => __("Xem thêm", "mytheme"),
                    'link' => $link
                );
                get_template_part('templates/content', 'viewmore-btn', $args);
            ?>
        </div>
    </div>
    <?php endif; ?>
    <?php $i++; endwhile; ?>
</section>
<?php endif; ?>