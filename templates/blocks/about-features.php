<section class="about-features relative py-20">
    <img src="<?php echo get_stylesheet_directory_uri() ?>/img/asset/about-bg.jpg" alt="Sứ mạng - Tầm nhìn - Giá trị cốt lõi" class="absolute top-0 left-0 w-full h-full object-cover" width="1409" height="648">
    <div class="wrapper relative">
        <?php if(get_field('vision_title')) : ?>
        <h2 class="font-bold text-3xl lg:text-5xl text-white text-center">
            <?php the_field('vision_title') ?>
        </h2>
        <?php endif; ?>
        <?php if(have_rows('vision_blocks')) : ?>
        <div class="features grid grid-cols-1 lg:grid-cols-3 gap-8 mt-10">
            <?php 
                while(have_rows('vision_blocks')) : the_row(); 
                $icon = get_sub_field('icon');
            ?>
            <div class="item text-center rounded-md overflow-hidden bg-white p-8 pt-[100px] relative">
                <div class="badge bg-tt-green w-[80px] h-[80px] rounded-br absolute top-0 left-0 flex items-center justify-center transition-colors">
                    <img class="h-8" src="<?php echo $icon['url'] ?>" alt="<?php echo $icon['alt'] ?>" loading="lazy" width="100" height="100">
                </div>
                <?php if(get_sub_field('title')) : ?>
                <h3 class="entry-title uppercase font-bold text-2xl transition-colors"><?php the_sub_field('title') ?></h3>
                <?php endif; ?>
                <?php if(get_sub_field('content')) : ?>
                <p class="text-sm mt-4"><?php the_sub_field('content') ?></p>
                <?php endif; ?>
            </div>
            <?php endwhile; ?>
        </div>
        <?php endif; ?>
    </div>
</section>