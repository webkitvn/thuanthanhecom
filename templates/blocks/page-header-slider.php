<?php if(have_rows('header_slider')) : ?>
<section class="page-header-slider relative overflow-x-hidden">
    <div class="swiper">
        <div class="swiper-wrapper">
            <?php 
                while(have_rows('header_slider')) : the_row(); 
                    $img = get_sub_field('image');
                    $title = get_sub_field('title');
                    $content = get_sub_field('content');
                    $link = get_sub_field('link');
            ?>
            <div class="swiper-slide flex flex-wrap lg:grid lg:grid-cols-3">
                <div class="text-box w-full lg:col-span-1 pt-6 pb-10 px-4 lg:p-6 lg:py-16 lg:pl-[50px] bg-tt-green text-white order-2 lg:order-none" data-mh="page-header-slider-text-box">
                    <?php if($title) : ?>
                    <h2 class="entry-title font-bold text-2xl lg:text-6xl lg:mb-4 !leading-[1.15]">
                        <?php echo $title; ?>
                    </h2>
                    <?php endif; ?>
                    <?php if($content) : ?>
                    <p class="lg:font-semibold line-clamp-3"><?php echo $content; ?></p>
                    <?php endif; ?>
                    <?php 
                        $args = array(
                            'text' => __("Xem thêm", "mytheme"),
                            'color'=>'white', 
                            'link'=> $link
                        );
                        get_template_part('templates/content', 'viewmore-btn', $args);
                    ?>
                </div>
                <div class="img-holder relative overflow-hidden w-full lg:col-span-2 order-1 lg:order-none pt-[66.66%] lg:pt-0">
                    <?php echo wp_get_attachment_image($img['id'], 'large', false, array('class'=>'absolute w-full h-full top-0 left-0 object-cover')) ?>
                </div>
            </div>
            <?php endwhile; ?>
        </div>
    </div>
    <div class="swiper-nav rounded-t-md bg-tt-green p-4 absolute bottom-0 right-[-5px] lg:right-[50px] flex gap-x-4 items-center z-20">
        <div class="swiper-button-prev after:content-none static w-[30px] h-[30px] lg:w-[40px] lg:h-[40px] rounded-full border border-current flex justify-center items-center text-white m-0 hover:text-tt-green">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                stroke="currentColor" class="w-6 h-6">
                <path stroke-linecap="round" stroke-linejoin="round" d="M10.5 19.5L3 12m0 0l7.5-7.5M3 12h18" />
            </svg>
        </div>
        <div class="swiper-button-next after:content-none static w-[30px] h-[30px] lg:w-[40px] lg:h-[40px] rounded-full border border-current flex justify-center items-center text-white m-0 hover:text-tt-green">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                stroke="currentColor" class="w-6 h-6">
                <path stroke-linecap="round" stroke-linejoin="round" d="M13.5 4.5L21 12m0 0l-7.5 7.5M21 12H3" />
            </svg>
        </div>
    </div>
</section>
<?php endif; ?>