<?php 
    if(get_field('post_cats')) :
        $cats = get_field('post_cats');
?>
<section class="py-10">
    <div class="wrapper">
        <?php 
            $args = array(
                'text' => __("Tin tức", "mytheme")
            );
            get_template_part('templates/content', 'section-title', $args);
        ?>
        <ul class="nav nav-tabs custom-nav-tabs flex lg:justify-center list-none border-b pl-0 mt-4 mb-4 w-full lg:w-fit mx-auto overflow-auto snap-x snap-mandatory"
            id="tabs-tab" role="tablist" data-te-nav-ref>
            <?php $i = 1; foreach($cats as $cat) : ?>
            <li class="nav-item block w-fit snap-start" role="presentation">
                <a href="#tabs-<?php echo sanitize_title($cat->name) ?>"
                    class="nav-link text-center text-sm lg:text-lg block w-fit whitespace-nowrap font-bold border-x-0 border-t-0 border-b-2 border-transparent px-6 py-3 hover:border-transparent hover:bg-gray-100 focus:border-transparent"
                    data-te-toggle="pill"
                    data-te-target="#tabs-<?php echo sanitize_title($cat->name) ?>" 
                    role="tab" 
                    <?php echo $i==1 ? 'data-te-nav-active' : '' ?>
                    aria-controls="tabs-<?php echo sanitize_title($cat->name) ?>"
                    aria-selected="<?php echo $i == 1 ? 'true' : 'false' ?>"><?php echo $cat->name ?></a>
                <?php $i++; endforeach; ?>
            </li>
        </ul>
        <div class="tab-content tab-posts tab-posts-grid mt-10" id="tabs-tabContent">
            <?php $i = 1; foreach($cats as $cat) : ?>
            <div class="hidden <?php echo $i == 1 ? 'opacity-100' : 'opacity-0' ?> transition-opacity duration-150 ease-linear data-[te-tab-active]:block"
                id="tabs-<?php echo sanitize_title($cat->name) ?>" 
                role="tabpanel"
                aria-labelledby="tabs-<?php echo sanitize_title($cat->name) ?>-tab"
                <?php echo $i==1 ? 'data-te-tab-active' : '' ?>>
                <div class="posts grid grid-cols-4 grid-rows-2 gap-8">
                    <?php 
                        $query = new WP_Query(array(
                            'posts_per_page' => 5,
                            'cat' => $cat->term_id,
                        ));
                        while($query->have_posts()) : $query->the_post();
                    ?>
                    <?php get_template_part('templates/post/content', 'post') ?>
                    <?php endwhile; wp_reset_query(); ?>
                </div>
                <div class="text-center mt-4">
                    <?php 
                        $args = array(
                            'text' => __("Xem thêm", "mytheme"),
                            'color'=>'tt-green', 
                            'link'=>array(
                                'url' => '/tin-tuc'
                            )
                        );
                        get_template_part('templates/content', 'viewmore-btn', $args);
                    ?>
                </div>
            </div>
            <?php $i++; endforeach; ?>
        </div>
    </div>
</section>
<?php endif; ?>