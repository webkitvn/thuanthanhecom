<section class="py-10 image-slider">
    <div class="wrapper text-center">
        <?php 
            if(get_field('home_about_title')){
                $args = array(
                    'text' => get_field('home_about_title')
                );
                get_template_part('templates/content', 'section-title', $args);
            }
        ?>
        <?php if(get_field('home_about_content')) : ?>
        <div class="entry-content max-w-2xl mx-auto mt-4">
            <p><?php the_field('home_about_content') ?></p>
        </div>
        <?php endif; ?>
        <?php 
            if(get_field('home_about_link')) {
                $args = array(
                    'text' => __("Xem thêm", "mytheme"),
                    'color' => 'tt-green',
                    'link' => get_field('home_about_link')
                );
                get_template_part('templates/content', 'viewmore-btn', $args);
            }
        ?>
    </div>
    <?php if(get_field('home_about_images')) :
        $images = get_field('home_about_images');
    ?>
    <div class="swiper mt-10 px-[30px] lg:px-60 relative" data-pp-lg="1" data-pp-sm='1'>
        <div class="swiper-wrapper">
            <?php foreach($images as $img) : ?>
            <div class="swiper-slide rounded-[10px] overflow-hidden">
                <?php echo wp_get_attachment_image($img['id'], 'large', false, array('class'=>'w-full block')) ?>
            </div>
            <?php endforeach; ?>
        </div>
        <div
            class="swiper-nav hidden rounded-tl-[10px] rounded-br-[10px] bg-tt-green p-4 absolute bottom-0 right-60 lg:flex gap-x-4 items-center z-20">
            <div
                class="swiper-button-prev after:content-none static w-[40px] h-[40px] rounded-full border border-current flex justify-center items-center text-white m-0 hover:text-tt-green">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                    stroke="currentColor" class="w-6 h-6">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M10.5 19.5L3 12m0 0l7.5-7.5M3 12h18" />
                </svg>
            </div>
            <div
                class="swiper-button-next after:content-none static w-[40px] h-[40px] rounded-full border border-current flex justify-center items-center text-white m-0 hover:text-tt-green">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                    stroke="currentColor" class="w-6 h-6">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M13.5 4.5L21 12m0 0l-7.5 7.5M21 12H3" />
                </svg>
            </div>
        </div>
    </div>
    <?php endif; ?>
</section>