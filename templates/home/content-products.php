<section class="products-slider py-10 bg-gray-50">
    <div class="wrapper text-center relative">
        <?php
            if(get_field('home_products_title')) {
                $args = array(
                    'text' => get_field('home_products_title')
                );
                get_template_part('templates/content', 'section-title', $args);
            }
        ?>
        <?php if(get_field('home_product_content')) : ?>
        <div class="entry-content max-w-2xl mx-auto mt-4">
            <p><?php the_field('home_products_content') ?></p>
        </div>
        <?php endif; ?>
    </div>
    <div class="wrapper">
        <?php 
            $productCats = get_terms(array(
                'taxonomy' => 'product_cat',
                'parent' => 0,
                'hide_empty' => true
            ));
            if($productCats) :
        ?>
        <ul class="nav nav-tabs custom-nav-tabs flex lg:justify-center list-none border-b pl-0 mt-4 mb-4 w-full lg:w-fit mx-auto snap-x snap-mandatory overflow-x-auto"
            id="tabs-tab" role="tablist" data-te-nav-ref>
            <?php $i = 1; foreach($productCats as $cat) : ?>
            <li class="nav-item snap-start" role="presentation">
                <a href="#tabs-<?php echo $cat->term_id ?>"
                    class="nav-link text-center text-sm lg:text-lg block w-fit whitespace-nowrap font-bold border-x-0 border-t-0 border-b-2 border-transparent px-6 py-3 hover:border-transparent hover:bg-gray-100 focus:border-transparent"
                    data-te-toggle="pill" data-te-target="#tabs-<?php echo $cat->term_id ?>" role="tab"
                    <?php echo $i==1 ? 'data-te-nav-active' : '' ?> aria-controls="tabs-<?php echo $cat->term_id ?>"
                    aria-selected="<?php echo $i == 1 ? 'true' : 'false' ?>"><?php echo $cat->name ?></a>
            </li>
            <?php $i++; endforeach; ?>
        </ul>
        <div class="tab-content tab-posts mt-10" id="tabs-tabContent">
            <?php $i = 1; foreach($productCats as $cat) : ?>
            <div class="hidden <?php echo $i == 1 ? 'opacity-100' : 'opacity-0' ?> transition-opacity duration-150 ease-linear data-[te-tab-active]:block"
                id="tabs-<?php echo $cat->term_id ?>" role="tabpanel"
                aria-labelledby="tabs-<?php echo $cat->term_id ?>-tab" <?php echo $i==1 ? 'data-te-tab-active' : '' ?>>

                <div class="swiper products mt-10">
                    <div class="swiper-wrapper">
                        <?php 
                            $query = new WP_Query(array(
                                'post_type' => 'product',
                                'posts_per_page' => 12,
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'product_cat',
                                        'field' => 'term_id',
                                        'terms' => $cat->term_id
                                    )
                                )
                            ));
                            while($query->have_posts()) : $query->the_post();
                        ?>
                        <div class="swiper-slide">
                            <?php wc_get_template_part('content', 'product') ?>
                        </div>
                        <?php endwhile; wp_reset_query(); ?>
                    </div>
                    <div class="swiper-pagination static lg:hidden"></div>
                </div>
                <div
                    class="swiper-button-prev hidden after:content-none w-[40px] h-[40px] rounded-full border border-current lg:flex justify-center items-center text-tt-green m-0 left-[-50px] translate-y-[-50%] hover:text-tt-green">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                        stroke="currentColor" class="w-6 h-6">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M10.5 19.5L3 12m0 0l7.5-7.5M3 12h18" />
                    </svg>
                </div>
                <div
                    class="swiper-button-next hidden after:content-none w-[40px] h-[40px] rounded-full border border-current lg:flex justify-center items-center text-tt-green m-0 right-[-50px] translate-y-[-50%] hover:text-tt-green">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                        stroke="currentColor" class="w-6 h-6">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M13.5 4.5L21 12m0 0l-7.5 7.5M21 12H3" />
                    </svg>
                </div>

            </div>
            <?php $i++; endforeach; ?>
        </div>
        <?php endif; ?>
        <div class="text-center">
            <?php 
            $link = array(
                'url' => get_post_type_archive_link( 'product' )
            );
            $args = array(
                'text' => __("Xem tất cả", "mytheme"),
                'color' => 'tt-green',
                'link' => $link
            );
        get_template_part('templates/content', 'viewmore-btn', $args) ?>
        </div>
</section>