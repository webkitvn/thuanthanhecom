<div class="post">
    <a href="<?php the_permalink() ?>" class="thumb bg-green-200 relative block rounded-md overflow-hidden">
        <div class="badge absolute top-0 left-0 bg-tt-green text-white text-sm text-center leading-none rounded-br z-20 py-2 px-3 transition-colors">
            <span class="m block"><?php the_time('M') ?></span>
            <span class="d block font-bold lg:text-2xl leading-none"><?php the_time('d') ?></span>
        </div>
        <?php the_post_thumbnail('post-thumbnail', array('class'=>'w-full h-full object-cover z-10')) ?>
    </a>
    <h3 class="entry-title mt-4 line-clamp-2 font-semibold transition-colors">
        <a href="<?php the_permalink() ?>"><?php the_title() ?></a>
    </h3>
</div>