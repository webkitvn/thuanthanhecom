<aside class="sidebar col-span-1">
    <div class="widget featured-posts posts-slider bg-gray-50 rounded-xl p-5">
        <h4 class="text-tt-green font-bold text-2xl mb-4"><?php _e("Tin nổi bật", "mytheme") ?></h4>
        <div class="swiper">
            <div class="swiper-wrapper">
                <?php
                    $query = new WP_Query(array(
                        'posts_per_page' => 5,
                        'post__in'            => get_option( 'sticky_posts' ),
                        'ignore_sticky_posts' => 1,
                    ));
                    while($query->have_posts()) : $query->the_post();
                ?>
                <div class="swiper-slide">
                    <?php get_template_part('templates/post/content', 'post') ?>
                </div>
                <?php endwhile; wp_reset_query(); ?>
            </div>
        </div>
        <div class="swiper-nav text-tt-green flex gap-x-4 mt-4 justify-center">
            <div
                class="swiper-button-prev after:content-none static w-[40px] h-[40px] rounded-full border border-current flex justify-center items-center text-tt-green m-0 hover:text-tt-green">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                    stroke="currentColor" class="w-6 h-6">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M10.5 19.5L3 12m0 0l7.5-7.5M3 12h18" />
                </svg>
            </div>
            <div
                class="swiper-button-next after:content-none static w-[40px] h-[40px] rounded-full border border-current flex justify-center items-center text-tt-green m-0 hover:text-tt-green">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                    stroke="currentColor" class="w-6 h-6">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M13.5 4.5L21 12m0 0l-7.5 7.5M21 12H3" />
                </svg>
            </div>
        </div>
    </div>

    <?php 
        global $post;
        $post_id = get_the_ID();
        $cat_ids = array();
        $categories = get_the_category( $post_id );
        if(!empty($categories) && !is_wp_error($categories)):
            foreach ($categories as $category):
                array_push($cat_ids, $category->term_id);
            endforeach;
        endif;
    
        $current_post_type = get_post_type($post_id);
    
        $query_args = array( 
            'category__in'   => $cat_ids,
            'post_type'      => $current_post_type,
            'post__not_in'    => array($post_id),
            'posts_per_page'  => 5,
        );
    
        $query = new WP_Query( $query_args );
        if($query->have_posts()) :
    ?>
    <div class="widget related-posts bg-gray-50 rounded-xl p-5 mt-6">
        <h4 class="text-tt-green font-bold text-2xl mb-4"><?php _e("Tin tức khác", "mytheme") ?></h4>
        <div class="posts grid grid-cols-1 gap-4">
            <?php
                while($query->have_posts()) : $query->the_post();
            ?>
            <?php get_template_part('templates/post/content', 'post') ?>
            <?php endwhile; wp_reset_query(); ?>
        </div>
    </div>
    <?php endif;?>
</aside>