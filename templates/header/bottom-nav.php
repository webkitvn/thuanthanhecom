<div class="bottom-nav pb-2 w-full gap-x-[2em] hidden lg:flex justify-end items-center">
    <?php 
        wp_nav_menu( array(
            'theme_location' => 'primary',
            'menu_class' => 'menu flex gap-x-[2em]',
            'container' => 'ul'
        ) )
    ?>
</div>