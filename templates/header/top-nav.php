<div class="top-nav gap-x-[2em] flex justify-between w-full lg:w-auto lg:justify-end items-center relative mb-2 lg:mb-0">
    <?php if(get_field('hotline', 'option')) : $hotline = get_field('hotline', 'option') ?>
    <div class="html-1 font-light text-tt-green text-xs lg:text-base">
        <?php _e("Hotline", "mytheme") ?>: <a href="tel:<?php echo $hotline['phone'] ?>" class="font-semibold"><b><?php echo $hotline['text'] ?></b></a>
    </div>
    <?php endif; ?>
    <div class="bg-tt-green text-white py-[1em] pr-[20px] items-center rounded-bl-2xl font-extralight text-sm relative">
        <div class="icons divide-x divide-white flex">
            <button class="search-btn border-0 bg-none p-0 px-3">
                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/search-btn.svg" alt="<?php _e("Tìm kiếm", "mytheme") ?>" class="!h-[16px] lg:!h-[20px]">
                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/close.svg" alt="<?php _e("Tìm kiếm", "mytheme") ?>" class="!h-[16px] lg:!h-[20px] hidden">
            </button>
            <div class="socials px-3 flex gap-3">
                <?php if(get_field('facebook', 'option')) : ?>
                <a href="<?php the_field('facebook', 'option') ?>">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/img/facebook.svg" alt="Facebook" class="!h-[16px] lg:!h-[20px]">
                </a>
                <?php endif; ?>
                <?php if(get_field('instagram', 'option')) : ?>
                <a href="<?php the_field('instagram', 'option') ?>">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/img/instagram.svg" alt="instagram" class="!h-[16px] lg:!h-[20px]">
                </a>
                <?php endif; ?>
            </div>
            <a href="<?php echo esc_url( wc_get_cart_url() ); ?>" class="cart-link block relative pl-3">
                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/cart.svg" alt="<?php _e("Giỏ hàng", "mytheme") ?>" class="!h-[16px] lg:!h-[20px]">
                <span class="cart-count count header-cart-count absolute top-0 right-[-20px] rounded-full bg-white text-tt-green !w-[16px] lg:!w-[20px] !h-[16px] lg:!h-[20px] flex justify-center items-center text-xs z-10">
                <?php echo WC()->cart->get_cart_contents_count(); ?>
                </span>
            </a>
        </div>
        <div class="search-form hidden">
            <form action="/">
                <input type="search" name="s" placeholder="<?php _e("Tìm kiếm...", "mytheme") ?>">
                <button type="submit"><?php _e("Tìm kiếm", "mytheme") ?></button>
            </form>
        </div>
    </div>
</div>